const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const {expect} = chai;
const {restore, stub} = sinon;

describe('Sample Testing', () => {
  beforeEach(() => {});
  afterEach(() => {
    restore();
  });

  describe('Verify Customer', () => {
    context('When customer is logged-in', () => {
      it('Should return success', async () => {
        const isloggedIn = true;
        expect(isloggedIn).to.be.eq(true);
      });
    });
    context('When customer token expired', () => {
      it('Should return to login page', async () => {
        const expired = true;
        expect(expired).to.be.eq(true);
      });
    });
  });

});